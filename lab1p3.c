#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

/*  reverseAndCapitalize function
    Purpose: Reverses each word, replaces '_' with space, and leaves non-alpha characters as they are
    Input:  Input will be read in from ./lab1p3in.txt, but this function takes no parameters
    Output: Creates a files ./lab1p3out.txt that will contain the resulting message(s)
*/
void reverseAndCapitalize() {

    char myString[100];
	FILE *fileInput, *fileOutput;
	fileInput = fopen("./lab1p3in.txt", "r");
    fileOutput = fopen("./lab1p3out.txt", "w");
    
    if (fileInput == NULL) {
        printf("The input file could not be found.\n");
    }


    while (fgets(myString, 100, fileInput) != NULL) {
        printf("The line being processed is: ");
        puts(myString); // put string in the buffer

        char *ptr = myString;
        char *start = myString;
        char *end = myString;
        int previousNotAlpha = 0; // 1 if previous char was alpha, 0 if not

        // ptr loops each character in the string
        while (*ptr) {
            if (isalpha(*ptr) != 0) {
                if (previousNotAlpha == 1) { // only reset start ptr when at a new word
                    start = ptr;
                }
                end = ptr;
                *end = toupper(*end); // capitalize
                previousNotAlpha = 0;
            } else {
                while (end != start) {
                    fputc(*end, fileOutput); // loop backwards, write chars to output as you go
                    end--;
                }

                if (isalpha(*start) != 0) // only write the char if alpha
                    fputc(*start, fileOutput);
                if (*ptr == '_') { // convert '_' to a space
                    fputc(' ', fileOutput);
                } else {
                    fputc(*ptr, fileOutput); // write the non-alpha char in its original position
                }
                start = ptr;
                end = ptr;
                previousNotAlpha = 1;
            }
            ptr++;
        }

        // loops through last word when it doesn't end in a non alpha like war_eagle (e is alpha)
        if (!*ptr && *end && *start) {
            while (end != start) {
                fputc(*end, fileOutput);
                end--;
            }
            if (isalpha(*start) != 0)
                fputc(*start, fileOutput);
        }
    }

    fclose(fileInput);
    fclose(fileOutput);

}

int main() {
    reverseAndCapitalize();
    printf("\n");
    return EXIT_SUCCESS;
}
